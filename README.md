# Open source high performance computing #

## About the Course ##
Scientific computing 문제들을 해결하기 위한 최신 동향의 컴퓨터 기술을 사용한 프로그래밍 중심의 코스입니다. 과학계산 분야에서 많이 사용하는 간단한 행렬연산을 예제로 사용하여, 버전컨트롤 시스템, MS Visual Studio를 사용한 소프트웨어 빌드와 디버깅, Open Source 를 활용한 High Performance computing 을 소개합니다.

##  Goal of Course ##
Solving Ax=b  with Open Source Library.

## Course Syllabus ##
* Version control systems. GIT with Bitbucket repositories.
* Using open source
* Building softwares with Microsoft Visual studio
* Thread and Processes. Basics of computer architecture.
* Debuggers, call stacks, unit tests

## Course Materials ##
아래 출처의 자료들을 발췌하여 사용.

* [Applied Mathematics 483/583 - High Performance Scientific Computing](http://faculty.washington.edu/rjl/classes/am583s2014/) at University of Washington
* [Atlassian GIT tutorials](https://www.atlassian.com/git/tutorials)
* [Getting Started with C++ in Visual Studio](https://msdn.microsoft.com/en-us/library/jj620919.aspx)

## Lecture slides ##
1. [Introduction, Environment setup, Hello World! by VC++](slides/oshpc_1.pdf)
1. [Version control system. Git, GitHub and Bitbucket](slides/oshpc_2.pdf)
1. [Demo of C and Git, Toy example. Newtown method](slides/oshpc_3.pdf)
1. [Intro to C, functions, arrays](slides/oshpc_4.pdf)
1. BLAS, LAPACK, Intel MKL
1. Computer architecture and memory optimization. Measuring computing performance.
1. Mini project. Solving linear system with Intel MKL.
1. Project presentation.

## Links ##
* [Visual Studio Community](https://www.visualstudio.com/en-us/downloads/download-visual-studio-vs.aspx)
* [Community Licensing for Intel Performance Libraries](https://software.intel.com/sites/campaigns/nest/)
* [Git](https://git-scm.com/downloads)
* [Bitbucket](https://bitbucket.org/)
* [GitHub](https://github.com/)