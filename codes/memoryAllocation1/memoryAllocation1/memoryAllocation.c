#include <stdio.h>
#include <stdlib.h>

void setvals(double *pDst, int n, double value);

int main()
{
    double* pX;
    int n = 1024;
    pX = malloc(n * sizeof(double));

    if (pX == NULL)
    {
        printf("Insufficient memory\n");
        return 0;
    }

    setvals(pX, n, 5.0);

    printf("Print first 5 elements\n");
    for (size_t i = 0; i < 5; i++)
    {
        printf("%f\n", pX[i]);
    }

    free(pX);

    return 0;
}

void setvals(double *pDst, int n, double value)
{
    for (int i = 0; i < n; i++)
    {
        pDst[i] = value;
    }
}